/* eslint-disable no-unused-vars */
// import React from 'react';
import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from 'react-router-dom';
import { Markup } from 'interweave';

// eslint-disable-next-line no-unused-vars
import Contacts from './components/contacts';
import './App.scss';

// import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { contacts: [] };
  }

  componentDidMount() {
    fetch('http://jsonplaceholder.typicode.com/users')
    .then((res) => res.json())
    .then((data) => {
      this.setState({ contacts: data });
    })
    .catch(console.log);
  }
  
  render() {
    return (
      <div className="parallax center">
        <Router>
          <div>
            <ul className="navbar font-indieflower">
              <li>
                <Link to="/">Accueil</Link>
              </li>
              <li>
                <Link to="/about">À propos</Link>
              </li>
            </ul>

            <Switch>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Router>
        {/* <Contacts contacts={this.state.contacts} /> */}
      </div>
    );
  }
}

function Home() {
  return (
    <div>
      <h2 className="font-indieflower">
        Projet Monstera
      </h2>
      <div>
        <Link to="/about">
          <button type="button" className="btn btn-info d-flex mx-auto font-weight-bold font-indieflower mt-2">
            Découvrir
          </button>
        </Link>
      </div>
    </div>
  );
}

function About() {
  const links = [
    { name: 'Discord du projet', href: 'https://discord.gg/zMfAaCc' },
    { name: 'Wiki du GitLab', href: 'https://gitlab.com/Norahenn/monstera-front/-/wikis/home' }
  ];
  const resume = "Alors que vous rentriez ches vous, vous trouvez un sac à votre porte. Le lendemain vous le déposer aux objets perdus en allant à votre travail. Etrangement, peut importe ce vous faisiez, il  revenait toujours à votre bas de porte.<br/>Dépité, vous décidez de fouiller le sac pour en savoir plus sur la situation. Vous ne trouvez que 2 barres vitaminées, 1 bouteille d'eau, un carnet avec un crayon à papier et un ordinateur portable. Pour être très franc, vous aviez déjà vu le contenu lors de vos précédentes tentatives de vous en débarasser.<br/>Cependant cette fois-ci, vous n'en pouvez plus et ne voulez vraiment ne plus revoir ce maudis sac.<br/>Pour ce faire, vous décidez de lire le carnet pour trouver le nom du propriétaire. Malheureusemen vous ne trouvez rien d'autre que des dessins de plantes et ce qui ressemble à un mot de passe. Vous décidez de l'essayer sur l'ordinateur, et avez réussir à ouvrir une des 2 sessions...";
  return (
    <div>
      <p className="w-50 mx-auto font-italic">
        <Markup content={resume} />
      </p>
      <div className="d-flex justify-content-center">
        <div>
          <h5 className="pb-2">
            <u>Liens utiles</u>
          </h5>
          <ul className="list-unstyled">
            {links.map((value, index) => (
              <li key={links.index} className="font-weight-bold">
                <a href={value.href} target="_blank" rel="noopener noreferrer">
                  {value.name}
                </a>
              </li>
        ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default App;
