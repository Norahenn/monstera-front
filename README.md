# Nom qui Claque

[![ReactJS 6.2](https://img.shields.io/badge/ReactJS-16.11-blue)](https://reactjs.org/)
[![NodeJS 11.15](https://img.shields.io/badge/NodeJS-11.15-green)](https://nodejs.org/en)

Hi there!

This repo is dedicated to the front part of the Nom qui Claque project. 

Nom qui Claque is a point & click game, the "you found a lost phone" type, though its a laptop this time.

If you wanna work on it, I'd recommend reading the whole thing, otherwise just read the [Install section](#install), maybe the [Pitch section](#pitch) to get some context, have fun!

Go to [Trello](https://trello.com/b/kql4iLm8/nom-qui-claque-front) to see the progress so far!

***
## Install me!

So far:

1. `git clone https://gitlab.com/Norahenn/nom-qui-claque.git`
2. `npm install`
3. Then you can:<br>
   **REACT**<br>
   *Feel free to look up react's [documentation](https://reactjs.org/docs/getting-started.html)*<br>
   *All of these can be found in the [react.md](react.md) file*
   1. `npm start` to start server at [http://localhost:3000](http://localhost:3000). The page will reload on edit, and you'll be able to see lints error in the console
   2. `npm test` launches the test runner in interactive mode, see the doc on [running tests](https://facebook.github.io/create-react-app/docs/running-tests) here<br>
   *Feel free to look upt jester's [documentation](https://jestjs.io/docs/en/getting-started)*
   3. `npm run build` to build the app and make it deploy ready! See the doc on [deployment](https://facebook.github.io/create-react-app/docs/deployment) to know more
   
    **STORYBOOK**<br>
    *Feel free to look storybook's [documentation](https://storybook.js.org/docs/basics/introduction/)*<br>
    1. `npm run storybook` to start storybook server at [http://localhost:9009](http://localhost:9009)

If you want it to work properly, you'll also need the back's repo and read the fairly similar README or trust the following instruction to do the work:

1. soon enough
2. soo enough

***
## Pitch

#Laptop #Mystery #Apocalypse #Plants #Dealer #Techy<3

As you came back home as usual, you found a bag at your front door. The next day you dropped it at the lost and found on your way to work. Weirdly enough, no matter what you did, the bag always came back to your front door.

Desperate, you decided to look into it to see what it was all about. You only found two lunch bars, a small bottle of water, a notebook with a graphite pen, and a computer. To be fair you've already seen the content before, as you tried to get rid of it. But this time, you're determined to find the owner and get done with it. You don't want to see that creepy bag ever again. To do so, you decide to look trough the notebook to find the owner's name. You didn't. You only found many plants drawing and what seemed to be a password. As you tried it out on the computer, it opened one of the two sessions...

[Spoils ahead!](https://gitlab.com/Norahenn/nom-qui-claque-front/wikis/home)